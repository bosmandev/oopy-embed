#!/usr/bin/python

# This script can be run directly (`python oopy_embed.py`).
# If you intend on using it often you may also install it as cli command:
# Local Install:
#   From top directory, run `pip install --user .`
#   This will install for your local user, so make sure ~/.local/bin is in your path.
# System Install
#   From top directory, run `pip install .`
#   This will install system-wide, for all users.
# Run with `oopy-embed`. Also see `oopy-embed --help`.


from typing import Optional

import shutil
import os
import sys
import argparse


default_tmp_dir = '/tmp/tmp.oopy-embed'
default_prefix = 'oe_'


def run(
        doc_file: str, 
        script_file: str, 
        output_file: Optional[str] = None, 
        tmp_dir: Optional[str] = None, 
        filename_prefix: Optional[str] = None
    ):

    tmp_dir = default_tmp_dir if tmp_dir is None else os.path.abspath(os.path.expanduser(tmp_dir))
    if filename_prefix is None: filename_prefix = default_prefix

    shutil.rmtree(tmp_dir, True)
    os.makedirs(tmp_dir, exist_ok=True)

    docfile_name = os.path.basename(os.path.expanduser(doc_file))
    docfile_dir = os.path.dirname(os.path.abspath(os.path.expanduser(doc_file)))
    docfile_path = os.path.join(docfile_dir, docfile_name)

    scriptfile_name = os.path.basename(os.path.expanduser(script_file))
    scriptfile_dir = os.path.dirname(os.path.abspath(os.path.expanduser(script_file)))
    scriptfile_path = os.path.join(scriptfile_dir, scriptfile_name)

    if output_file is None:
        output_dir = docfile_dir 
        newdoc_name = filename_prefix + docfile_name
    else:
        output_dir = os.path.dirname(os.path.abspath(os.path.expanduser(output_file)))
        newdoc_name = os.path.basename(os.path.abspath(os.path.expanduser(output_file)))

    newdoc_path = os.path.join(output_dir, newdoc_name)

    tmp_scripts_dir = os.path.join(tmp_dir, 'Scripts/python')

    shutil.unpack_archive(docfile_path, tmp_dir, 'zip')

    if not os.path.isdir(tmp_scripts_dir):
        os.makedirs(tmp_scripts_dir, exist_ok=True)

    shutil.copyfile(scriptfile_path, os.path.join(tmp_scripts_dir, scriptfile_name))

    manifest = []

    with open(os.path.join(tmp_dir, 'META-INF/manifest.xml')) as f:

        for line in f:
            if '</manifest:manifest>' in line:
                for path in ['Scripts/','Scripts/python/',f'Scripts/python/{scriptfile_name}']:
                  manifest.append(f' <manifest:file-entry manifest:media-type="application/binary" manifest:full-path="{path}"/>\n')
            manifest.append(line)

    with open(os.path.join(tmp_dir, 'META-INF/manifest.xml'), 'w') as f:
        f.write(''.join(manifest))

    shutil.make_archive(newdoc_path, 'zip', tmp_dir + os.sep)

    os.rename(newdoc_path + '.zip', newdoc_path)

    print(f'File with embedded script created: {newdoc_path}')


def main():
    parser = argparse.ArgumentParser(
        description = 'oopy-embed injects python script files (macros) into your OOo doc.',
        epilog      = """""",
        formatter_class = argparse.RawDescriptionHelpFormatter
    )

    required = parser.add_argument_group('required arguments')

    required.add_argument(
        '-D', '--doc-file',
        required = True,
        metavar = 'FILE_PATH',
        type = str,
        help = 'location of the input OOo/Libre doc file'
    )

    required.add_argument(
        '-S','--script-file',
        required = True,
        metavar = 'FILE_PATH',
        type = str,
        help = 'location of the python script (macro) file'
    )

    parser.add_argument(
        '-O','--output-file',
        metavar = 'FILE_PATH',
        type = str,
        help = 'output file; original file location and name (with prefix) used if not specified'
    )

    parser.add_argument(
        '-T','--tmp-dir',
        metavar = 'DIR_PATH',
        type = str,
        default = default_tmp_dir,
        help = 'temporary extraction/rebuilding location, default: %(default)s'
    )

    parser.add_argument(
        '-P','--filename-prefix',
        metavar = 'PREFIX',
        type = str,
        default = default_prefix,
        help = 'output file name prefix - used only if output file not specified, default: \'%(default)s\''
    )

    run(**vars(parser.parse_args(sys.argv[1:])))


if __name__ == '__main__':
    main()
