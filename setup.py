from setuptools import setup, find_packages


def readme():
    with open('README.md') as f:
        return f.read()


setup(
    name="oopy-embed",
    version="0.0.1",
    description="Embed python macro script files into Open Office docs.",
    long_description=readme(),
    long_description_content_type="text/markdown",
    classifiers=[
        "Programming Language :: Python :: 3.9",
        "License :: XXXXXXX :: XXXXXXXXXXX",
        "Operating System :: Linux",
        "Development Status :: 1 - Planning",
    ],
    keywords="open libre office docs embed macro python script",
    url="https://bitbucket.org/bosmandev/oopy-embed",
    author="Chris Bosman",
    author_email="chris.bosman@protonmail.com",
    license="XXX",
    packages=find_packages(),
    install_requires=[],
    entry_points={
        "console_scripts": ["oopy-embed=oopy_embed.oopy_embed:main"],
    },
    python_requires=">=3.7",
)
