__oopy-embed__ is a very simple program that injects python script files (macros) into your OOo docs. The same can 
easily be achieved by unpacking the doc manually, creating some directories, editing some lines in the manifest, 
copying the macros over and zipping it back up.

The _oopy_embed/oopy_embed.py_ script can be run directly (`python oopy_embed.py` or `./oopy-embed.py`).

If you intend on using it often you may also install it:

_NOTE: oopy-embed runs on Linux and has no non-native python dependencies. It was tested with 
Python 3.9 but should work with any Python version > 3._  

`git clone https://bitbucket.org/bosmandev/oopy-embed`  
`cd oopy-embed`

Local Install:  
`pip install --user .`  
This will install for your local user, so make sure _~/.local/bin_ is in your path.  

System Install (run as root):  
`pip install .`  
This will install system-wide, for all users.  

The following arguments are required: `-D/--doc-file, -S/--script-file`  

Example:  
    `oopy-embed -D ~/Documents/my_spreadsheet.ods -S ./macros/python/my_macro.py`  
    Resulting doc with macro embedded: _~/Documents/oe_my_spreadsheet.ods_

See `oopy-embed --help`

```
usage: oopy-embed [-h] -D FILE_PATH -S FILE_PATH [-O FILE_PATH] [-T DIR_PATH] [-P PREFIX]

oopy-embed injects python script files (macros) into your OOo doc.

optional arguments:
  -h, --help                                show this help message and exit
  -O FILE_PATH, --output-file FILE_PATH     output file; original file location and name (with prefix) used if not specified
  -T DIR_PATH, --tmp-dir DIR_PATH           temporary extraction/rebuilding location, default: /tmp/tmp.oopy-embed
  -P PREFIX, --filename-prefix PREFIX       output file name prefix - used only if output file not specified, default: 'oe_'

required arguments:
  -D FILE_PATH, --doc-file FILE_PATH        location of the input OOo/Libre doc file
  -S FILE_PATH, --script-file FILE_PATH     location of the python script (macro) file

```
---

Notes on creating python macros:

```python
# No need to manually install Python on non-Linux target systems where the resulting doc 
# is to be used; Libre Office for Windows and MAC have their own built-in Python 
# interpreter.
#
# Resulting doc after embedding can be used on any OS if the macro supports it.
#
# On linux, for testing/developing (testing the macro without embedding the script into 
# the calc file or copying it to the macros directory), run calc like so:
# soffice --calc --accept="socket,host=localhost,port=2002;urp;StarOffice.ServiceManager"
# then test your script from anywhere using the system's default python interpreter (or 
# the built-in interpreter if testing on Windows or MAC). 

# To get a context, do the following:

import uno
import socket

if __name__ == '__main__':
    # For testing (run script from anywhere, ex: your macro project directory)
    localContext = uno.getComponentContext()
    resolver = localContext.ServiceManager.createInstanceWithContext('com.sun.star.bridge.UnoUrlResolver', localContext)
    ctx = resolver.resolve('uno:socket,host=localhost,port=2002;urp;StarOffice.ComponentContext')
    smgr = ctx.ServiceManager
    doc = smgr.createInstanceWithContext('com.sun.star.frame.Desktop', ctx)
else:
    # For embedded script, context can be acquired as follows:
    doc = XSCRIPTCONTEXT.getDesktop()
    
```

See this good source for getting into python macros for OOo: http://christopher5106.github.io/office/2015/12/06/openoffice-libreoffice-automate-your-office-tasks-with-python-macros.html
